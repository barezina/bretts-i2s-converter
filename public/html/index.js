angular.module('bretts-converter', ['ngRoute'])
.config(function($routeProvider) {

    $routeProvider
    .when("/", {
        templateUrl : "developer-info/developer-info.html"
    })
    .when("/xml-upload", {
        templateUrl : "xml-upload/xml-upload.html"
    })

});