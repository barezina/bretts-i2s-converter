<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Cache;
use Illuminate\Http\Response;
use SoapBox\Formatter\Formatter;
use GuzzleHttp\Client;

class GeneralController extends Controller
{

    public $clientID = '';
    public $clientSecret = '';
    public $redirectURL = 'http://localhost:20000/spotify-callback';
    public $scopes = 'user-read-private user-read-email playlist-read-private playlist-modify-private playlist-modify-public playlist-read-collaborative';
    public $accessToken = '';
    public $spotifyUserID = '';

    public function frontendRedirect() {
        return redirect('/html');
    }

    public function itunesUpload(Request $request) {

        /*
        $file = $request->file('itunesplaylistxml');

        // Move Uploaded File
        $destinationPath = 'uploads';
        $file->move($destinationPath, $file->getClientOriginalName());
        */

        // $itunesXMLString = file_get_contents('uploads/' . $file->getClientOriginalName());

        $itunesXMLString = file_get_contents('uploads/Library.xml');
        $itunesArray = Formatter::make($itunesXMLString, Formatter::XML)->toArray();

        $niceSanitisedList = [];

        $playlistNamesToAvoid = [
            "Library",
            "Downloaded",
            "Music",
            "Movies",
            "TV Shows",
            "Podcasts",
            "Audiobooks",
            "Purchased",
            "Recently Added",
            "Recently Played"
        ];

        // Get the playlist objects
        $playlistObjects = $itunesArray['dict']['array']['dict'];

        // Get all the track indexes
        $libraryTrackIndex = $itunesArray['dict']['dict']['key'];

        // Get all the track objects
        $libraryTrackObjects = $itunesArray['dict']['dict']['dict'];

        // Get all the names for the playlists
        foreach($playlistObjects as $index => $playlistObject) {

            $playlistName = $playlistObject['string'][0];
            Log::info($index . ' : ' . $playlistName);

            if($index < 77) {
                continue;
            }

            if(in_array($playlistName, $playlistNamesToAvoid)) {
                continue;
            }

            // Get all the track IDs for this playlist
            $trackIdsForThisPlaylist = [];

            if(isset($playlistObject['array']['dict'])) {
                foreach ($playlistObject['array']['dict'] as $playlistTrackIndex) {
                    if (isset($playlistTrackIndex['integer'])) {
                        $trackIdsForThisPlaylist[] = $playlistTrackIndex['integer'];
                    }
                }
            }

            // If there is nothing in this playlist, just continue
            if(count($trackIdsForThisPlaylist) === 0) {
                continue;
            }

            // Now, for every track index, go look up the track object.

            $songsToAddToSantitisedPlaylist = [];
            $spotifyTrackIDsForThisPlaylist = [];

            foreach($trackIdsForThisPlaylist as $trackId) {

                $actualTrackIndex = array_search($trackId, $libraryTrackIndex);
                $trackObject = $libraryTrackObjects[$actualTrackIndex]['string'];

                if(count($trackObject) < 2) {
                    continue;
                }

                // Now we have the track object, try and find the same
                // track ID in spotify.

                $spotifyTrackId = '';

                $spotifyTrackObject = $this->searchTrack($trackObject[0], $trackObject[1]);

                if(isset($spotifyTrackObject['tracks']['items'][0]['id'])) {
                    $spotifyTrackId = $spotifyTrackObject['tracks']['items'][0]['id'];
                } else {
                    continue;
                }

                $spotifyTrackIDsForThisPlaylist[] = 'spotify:track:' . $spotifyTrackId;
            }

            // Now we have done all the tracks, add the nicely named playlist
            // along with it's santised songs to the proper array.

            $spotifyPlaylistID = $this->createPlaylist($playlistName)['id'];
            $trackChunksBy100 = array_chunk($spotifyTrackIDsForThisPlaylist, 100);

            foreach($trackChunksBy100 as $chunk) {
                $this->addSongsToPlaylist($spotifyPlaylistID, $chunk);
            }

        }

        return [];
    }

    public function me() {

        $client = new Client();
        $response = $client->request(
            'GET',
            'https://api.spotify.com/v1/me',
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . Cache::get('accessToken')
                ]
            ]
        );

        return json_decode($response->getBody(), true)['id'];
    }

    public function searchTrack($track, $artist) {

        Log::info('track: ' . $track . ' - ' . $artist);

        $searchString = '?q=' .
            'track:' .
            urlencode($track) .
            '%20' .
            'artist:' .
            urlencode($artist) .
            '&type=track' .
            '&limit=1';


        $client = new Client();
        try {
            $response = $client->request(
                'GET',
                'https://api.spotify.com/v1/search' . $searchString,
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . Cache::get('accessToken')
                    ]
                ]
            );
        } catch (\Exception $e) {

            // Just try again...
            Log::info('ERROR, RETRYING');
            $response = $client->request(
                'GET',
                'https://api.spotify.com/v1/search' . $searchString,
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . Cache::get('accessToken')
                    ]
                ]
            );
        }

        return json_decode($response->getBody(), true);
    }

    public function createPlaylist($name) {

        $client = new Client();
        $response = $client->request(
            'POST',
            'https://api.spotify.com/v1/users/brett083/playlists',
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . Cache::get('accessToken'),
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    'name' => $name
                ]
            ]
        );

        return json_decode($response->getBody(), true);
    }

    public function addSongsToPlaylist($playlistID, $songIDs) {

        $client = new Client();
        $response = $client->request(
            'POST',
            'https://api.spotify.com/v1/users/brett083/playlists/' . $playlistID . '/tracks',
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . Cache::get('accessToken'),
                    'Content-Type' => 'application/json'
                ],
                'json' => [
                    'uris' => $songIDs
                ]
            ]
        );

        return json_decode($response->getBody(), true);

    }

    public function spotifyAuthorise() {

        // Redirect to spotify's login page.

        $spotifyAuthoriseUrl =
        'https://accounts.spotify.com/authorize' .
        '?response_type=code' .
        '&client_id=' . $this->clientID .
        '&scope=' . $this->scopes .
        '&redirect_uri=' . $this->redirectURL;

        return redirect($spotifyAuthoriseUrl);

    }

    public function spotifyCallback(Request $request) {

        $callbackCode = $request->get('code');
        $client = new Client();
        $response = $client->request(
            'POST',
            'https://accounts.spotify.com/api/token',
            [
                'form_params' => [
                    'client_id' => $this->clientID,
                    'client_secret' => $this->clientSecret,
                    'grant_type' => 'authorization_code',
                    'code' => $callbackCode,
                    'redirect_uri' => $this->redirectURL
                ]
            ]
        );

        $arrayResponse = json_decode($response->getBody(), true);
        $this->accessToken = $arrayResponse['access_token'];
        Cache::add('accessToken', $this->accessToken, 30);
        $username = $this->me();
        Cache::add('me', $username, 30);
        return redirect('/upload');
    }


}
